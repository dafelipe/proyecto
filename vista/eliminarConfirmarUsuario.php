<?php
	include ("../modelo/deleteUser.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "scripts.php"?>
	<link rel="stylesheet" href="css/styleDeleteConfirmUser.css">
    <title>Eliminar Usuario</title>
	
</head>
<body>
	<?php include "header.php"?>
	<section id="container">
		<h1>Eliminar Usuario</h1>

		<div class="data_delete">
		<h2>¿Esta seguro de eliminar el siguiente registro?</h2>
		<p>Nombre : <span><?php echo $nombre ?></span></p>
		<p>Usuario : <span><?php echo $usuario ?></span></p>
		<p>Tipo Usuario : <span><?php echo $rol ?></span></p>

		<form method="post" action="">
			<input type="hidden" name="idusuario" value="<?php echo $idusuario ; ?>">
			<a href="lista_usuario.php" class="btn_cancel">Cancelar</a>
			<input type="submit" value="Aceptar" class="btn_ok">
		</form>

		</div>
	</section>

	<?php include "footer.php"?>
</body>
</html>