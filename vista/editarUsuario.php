<?php
    include ("../modelo/updateUsuario.php");
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Actualizar Usuario</title>
    <?php include "scripts.php"?>
    <link rel="stylesheet" href="css/styleUpdateUser.css">
    <link rel="stylesheet" href="css/alertUpdateUser.css">

</head>
<body>
	<?php include "header.php"?>
	<section id="container">

        <div class="form_register">

                <h1>Actualizar  usuario</h1>
                <hr>
                <div class="alert"><?php echo isset($alert) ? $alert : '' ?></div>

            <form action="" method="post">
                <input type="hidden" name="idUsuario" value="<?php echo $iduser; ?>">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" id="nombre" placeholder="Nombre completo" value="<?php echo $nombre;?>">
                <label for="correo">Correo electronico</label>
                <input type="email" name="correo" id="correo" placeholder="Correo electronico" value="<?php echo $correo;?>">
                <label for="usuario">Usuario</label>
                <input type="text" name="usuario" id="usuario" placeholder="Usuario" value="<?php echo $usuario;?>">
                <label for="clave">Clave</label>
                <input type="password" name="clave" id="clave" placeholder="Clave de acceso">
                <label for="rol">Tipo Usuario</label>
                <?php   include("../controlador/actualizarUsuarioController.php") ?>
                </select>
                <input type="submit" value="Actualizar usuario" class="btn_save">
            </form>
    
        </div>

	</section>

	<?php include "footer.php"?>

</body>
</html>