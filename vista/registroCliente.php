<?php
    include ("../conexion.php");
    include ("../modelo/registroCliente.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Registro Cliente</title>
    <?php include "scripts.php"?>
    <link rel="stylesheet" href="css/style_registroUser.css">
    <link rel="stylesheet" href="css/alert_registroUser.css">

</head>
<body>
	<?php include "header.php"?>
	<section id="container">

        <div class="form_register">

                <h1>Registro Cliente</h1>
                <hr>
                <div class="alert"><?php echo isset($alert) ? $alert : '' ?></div>

            <form action="" method="post">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" id="nombre" placeholder="Nombre completo">
                <label for="cedula">Cedula</label>
                <input type="number" name="cedula" id="cedula" placeholder="Numero de cedula">
                <label for="celular">Celular</label>
                <input type="number" name="celular" id="celular" placeholder="Celular">
                <label for="rol">Ciudad</label>
                <?php include ("../controlador/controllerRegistroCliente.php")?>
                </select>
                <input type="submit" value="Guardar Cliente" class="btn_save">
            </form>
    
        </div>

	</section>

	<?php include "footer.php"?>
</body>
</html>