<?php
    include ("../conexion.php");
    include ("../modelo/registroUsuario.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Registro Usuario</title>
    <?php include "scripts.php"?>
    <link rel="stylesheet" href="css/style_registroUser.css">
    <link rel="stylesheet" href="css/alert_registroUser.css">

</head>
<body>
	<?php include "header.php"?>
	<section id="container">

        <div class="form_register">

                <h1>Registro usuario</h1>
                <hr>
                <div class="alert"><?php echo isset($alert) ? $alert : '' ?></div>

            <form action="" method="post">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" id="nombre" placeholder="Nombre completo">
                <label for="correo">Correo electronico</label>
                <input type="email" name="correo" id="correo" placeholder="Correo electronico">
                <label for="usuario">Usuario</label>
                <input type="text" name="usuario" id="usuario" placeholder="Usuario">
                <label for="clave">Clave</label>
                <input type="password" name="clave" id="clave" placeholder="Clave de acceso">
                <label for="rol">Tipo Usuario</label>
                <?php include ("../controlador/registroUsuarioController.php")?>
                </select>
                <input type="submit" value="Crear usuario" class="btn_save">
            </form>
    
        </div>

	</section>

	<?php include "footer.php"?>
</body>
</html>