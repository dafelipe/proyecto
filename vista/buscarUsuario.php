<?php
	include ("../conexion.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "scripts.php"?>
	<link rel="stylesheet" href="css/Style_listaUsuario.css">
	<link rel="stylesheet" href="css/styleSearch.css">
	<title>Lista de usuarios</title>
	
	
</head>
<body>
	<?php include "header.php"?>
	<section id="container">

        <?php
        
            $busqueda = strtolower($_REQUEST['busqueda']);

            if (empty($busqueda)) {
                header("location: lista_usuario.php");
            }
        
        
        ?>

		<h1>Lista de usuarios</h1>
		<a href="registro_usuario.php" class="btn_new">Crear usuario</a>

		<form action="buscarUsuario.php" method="get" class="form_search"> 
			<input type="text" name="busqueda" id="busqueda" placeholder="Buscar" value="<?php echo $busqueda; ?>">
			<input type="submit" value="Buscar" class="btn_search">


		</form>


		<table>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Correo</th>
				<th>Usuario</th>
				<th>Rol</th>
				<th>Acciones</th>
			</tr>
			<?php include ("../controlador/listarUsuariosController.php");?>	
		</table>

	</section>
	<?php include "footer.php"?>
</body>
</html>