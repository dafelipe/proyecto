<?php
    include ("../modelo/updateCliente.php");
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Actualizar Usuario</title>
    <?php include "scripts.php"?>
    <link rel="stylesheet" href="css/styleUpdateUser.css">
    <link rel="stylesheet" href="css/alertUpdateUser.css">

</head>
<body>
	<?php include "header.php"?>
	<section id="container">

        <div class="form_register">

                <h1>Actualizar Cliente</h1>
                <hr>
                <div class="alert"><?php echo isset($alert) ? $alert : '' ?></div>

            <form action="" method="post">
                <input type="hidden" name="idcliente" value="<?php echo $iduser; ?>">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" id="nombre" placeholder="Nombre completo" value="<?php echo $nombre;?>">
                <label for="cedula">Cedula</label>
                <input type="text" name="cedula" id="cedula" placeholder="Cedula" value="<?php echo $correo;?>">
                <label for="celular">Celular</label>
                <input type="text" name="celular" id="celular" placeholder="Celular" value="<?php echo $usuario;?>">
                <label for="ciudad">Ciudad</label>
                <?php   include("../controlador/actualizarCliente.php") ?>
                </select>
                <input type="submit" value="Actualizar cliente" class="btn_save">
            </form>
    
        </div>

	</section>

	<?php include "footer.php"?>

</body>
</html>