<?php
session_start();

	include ("../conexion.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "scripts.php"?>
	<link rel="stylesheet" href="css/Style_listaUsuario.css">
	<link rel="stylesheet" href="css/styleSearch.css">
	<title>Lista de usuarios</title>
	
	
</head>
<body>
	<?php include "header.php"?>
	<section id="container">
		<h1>Lista de cliente</h1>
		<a href="registroCliente.php" class="btn_new">Crear cliente</a>
		
		<table>
			<tr>
				<th>ID</th>
				<th>Cedula</th>
				<th>Nombre</th>
                <th>Celular</th>
                <th>Ciudad</th>
				<th>Acciones</th>
			</tr>
			<?php include ("../controlador/listarCliente.php");?>	
		</table>

	</section>
	<?php include "footer.php"?>
</body>
</html>