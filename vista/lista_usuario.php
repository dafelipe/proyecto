<?php
session_start();

if ($_SESSION['rol'] != 1) {

    header("location: ../vista/index.php");
}

	include ("../conexion.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "scripts.php"?>
	<link rel="stylesheet" href="css/Style_listaUsuario.css">
	<link rel="stylesheet" href="css/styleSearch.css">
	<title>Lista de usuarios</title>
	
	
</head>
<body>
	<?php include "header.php"?>
	<section id="container">
		<h1>Lista de usuarios</h1>
		<a href="registro_usuario.php" class="btn_new">Crear usuario</a>
		
		<table>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Correo</th>
				<th>Usuario</th>
				<th>Rol</th>
				<th>Acciones</th>
			</tr>
			<?php include ("../controlador/listarUsuariosController.php");?>	
		</table>

	</section>
	<?php include "footer.php"?>
</body>
</html>