<?php

//Listar usuario

$query = pg_query($conexion,"SELECT u.id_usuario,u.nombre,u.correo,u.usuario,r.rol FROM usuarios u
JOIN rol r ON u.rol=r.id_rol ORDER BY id_usuario;");

$result = pg_num_rows($query);

if ($result > 0) {
    while ($data = pg_fetch_array($query)) {
        ?>
        <tr>
    <td><?php echo $data['id_usuario']; ?></td>
    <td><?php echo $data['nombre']; ?></td>
    <td><?php echo $data['correo']; ?></td>
    <td><?php echo $data['usuario']; ?></td>
    <td><?php echo $data['rol']; ?></td>
    <td>
        <a class="link_edit" href="editarUsuario.php?id=<?php echo $data['id_usuario']; ?>">Editar</a>
        |
        <a class="link_delete" href="eliminarConfirmarUsuario.php?id=<?php echo $data['id_usuario']; ?>">Eliminar</a>
    </td>
</tr>
<?php
    }
}
?>	