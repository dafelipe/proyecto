CREATE TABLE rol (
	id_rol int8 NOT NULL,
	rol varchar(25)
);
ALTER TABLE rol ADD CONSTRAINT pk_rol PRIMARY KEY(id_rol);
CREATE TABLE usuarios (
	id_usuario serial NOT NULL,
	nombre varchar(100),
	correo varchar(100),
	usuario varchar(15),
	clave varchar(10),
	rol int8
);
ALTER TABLE usuarios ADD CONSTRAINT pk_usuario PRIMARY KEY(id_usuario);
CREATE TABLE ciudad (
	id_ciudad serial NOT NULL,
	nombre_ciu varchar(50)
);
ALTER TABLE ciudad ADD CONSTRAINT pk_ciudad PRIMARY KEY(id_ciudad);
CREATE TABLE furgon (
	placas varchar(10) NOT NULL,
	conductor int8,
	capacidad int8
);
ALTER TABLE furgon ADD CONSTRAINT pk_furgon PRIMARY KEY(placas);
CREATE TABLE factura (
	num_factura serial NOT NULL,
	fecha timestamp DEFAULT CURRENT_TIMESTAMP,
	total int8,
	id_cliente int8,
	id_envio int8,
	metodo_pago int8
);
ALTER TABLE factura ADD CONSTRAINT pk_factura PRIMARY KEY(num_factura);
CREATE TABLE metodo_pago (
	id_metodopago int8 NOT NULL,
	tipo varchar(50)
);
ALTER TABLE metodo_pago ADD CONSTRAINT pk_pago PRIMARY KEY(id_metodopago);
CREATE TABLE cliente (
	idcliente serial NOT NULL,
	cedula int8,
	nombre varchar(255),
	celular int8,
	ciudad int8,
	fecha timestamp DEFAULT CURRENT_TIMESTAMP,
	usuario_id int8
);
ALTER TABLE cliente ADD CONSTRAINT pk_cliente PRIMARY KEY(idcliente);
CREATE TABLE envio (
	idenvio serial NOT NULL,
	descripcion varchar(255),
	valor_aprox int8,
	foto text,
	cliente int8,
	destino int8,
	origen int8,
	factura int8
);
ALTER TABLE envio ADD CONSTRAINT pk_envio PRIMARY KEY(idenvio);
ALTER TABLE usuarios ADD CONSTRAINT fk_rol FOREIGN KEY (rol) REFERENCES rol(id_rol) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE furgon ADD CONSTRAINT conduce FOREIGN KEY (conductor) REFERENCES usuarios(id_usuario) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE factura ADD CONSTRAINT recibe FOREIGN KEY (id_cliente) REFERENCES usuarios(id_usuario) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE factura ADD CONSTRAINT pago FOREIGN KEY (metodo_pago) REFERENCES metodo_pago(id_metodopago) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE envio ADD CONSTRAINT fk_envio FOREIGN KEY (cliente) REFERENCES cliente(idcliente) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE envio ADD CONSTRAINT fk_factura FOREIGN KEY (factura) REFERENCES factura(num_factura) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE cliente ADD CONSTRAINT registrador FOREIGN KEY (usuario_id) REFERENCES usuarios(id_usuario) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE cliente ADD CONSTRAINT residencia FOREIGN KEY (ciudad) REFERENCES ciudad(id_ciudad) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE envio ADD CONSTRAINT origen FOREIGN KEY (origen) REFERENCES ciudad(id_ciudad) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE envio ADD CONSTRAINT destino FOREIGN KEY (destino) REFERENCES ciudad(id_ciudad) ON DELETE RESTRICT ON UPDATE RESTRICT;

INSERT INTO rol (id_rol,rol) VALUES (1,'Administrador'), (2,'Empleado');

INSERT INTO usuarios (nombre,correo,usuario,clave,rol) VALUES ('Admin','admin@gmail.com','admin','admin',1);

INSERT INTO ciudad (nombre_ciu) values ('Villavicencio'), ('Bogota');

CREATE MATERIALIZED VIEW users AS SELECT u.id_usuario,u.nombre,u.correo,u.usuario,r.rol FROM usuarios u
JOIN rol r ON u.rol=r.id_rol ORDER BY id_usuario;