<?php
require ("modelo/iniciarSesion.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login | Envios </title>
    <link rel="stylesheet" type="text/css" href="vista/css/styleLogin.css">
</head>
<body>
    <section id = "container">
        <form action="" method="post">
            <h3>Iniciar Sesion</h3>
            <img src="vista/img/login(1).png" alt="Login">
            
            <input type="text" name ="usuario" placeholder="Usuario">
            <input type="password" name="clave" placeholder="Contraseña">
            <div class="alert"><?php echo isset($alert) ? $alert : "" ; ?></div>
            <input type="submit" value="INGRESAR">


        </form>
    
    </section>
</body>
</html>